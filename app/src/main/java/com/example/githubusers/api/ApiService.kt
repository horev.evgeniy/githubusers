package com.example.githubusers.api

import com.example.githubusers.User
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path




interface ApiService{
    @GET("/users")
    suspend fun getUsers(): List<User>

    @GET("/users/{username}")
    suspend fun getUser(@Path("username")username: String): User


    companion object{
        private const val BASE_URL ="https://api.github.com"
        fun create(): ApiService{
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()

            return retrofit.create(ApiService::class.java)
        }
    }

}
