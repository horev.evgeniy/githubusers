package com.example.githubusers

import android.app.Application
import android.util.Log
import androidx.lifecycle.*
import com.example.githubusers.DB.AppDatabase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor (private val repository: Repository) : ViewModel() {

    private val _users = MutableLiveData<Resource<List<User>>>(Resource.Loading())
    val users: LiveData<Resource<List<User>>> = _users

    fun load() {
        viewModelScope.launch() {

            try {
                val result = repository.getUsers()
                repository.insertUsers(result)
                _users.value = Resource.Success(result)
            } catch (exception: Exception) {
                Log.v("net error", exception.stackTraceToString())

                val dbResult = repository.getUsersFromDb()
                if (dbResult.isEmpty()) {
                    _users.value = Resource.DbError("no data in db")
                } else {
                    _users.value = Resource.NetError(exception, dbResult)
                }
            }
        }
    }

    init {
        load()
    }
}




