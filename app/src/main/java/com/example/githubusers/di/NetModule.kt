package com.example.githubusers.di

import com.example.githubusers.api.ApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
class NetModule {
    @Provides
    fun provideApiService(): ApiService{
        return ApiService.create()
    }
}