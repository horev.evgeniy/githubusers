package com.example.githubusers.di

import android.content.Context
import com.example.githubusers.DB.AppDatabase
import com.example.githubusers.DB.UserDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class DbModule {

    @Singleton
    @Provides
    fun provideAppDatabase(@ApplicationContext context: Context): AppDatabase{
        return AppDatabase.getInstance(context)
    }

    @Provides
    fun providesUserDao(appDatabase: AppDatabase): UserDao{
        return appDatabase.userDao()
    }
}