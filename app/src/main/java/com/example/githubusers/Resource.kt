package com.example.githubusers

import java.lang.Exception

sealed class Resource<out T>{
    class Loading<out T>: Resource<T>()
    class Success<out T>( val data: T): Resource<T>()
    class DbError<out T>(val dbMessage: String? = null) : Resource<T>()
    class NetError<T>(val message: Exception) : Resource<T>(){
        var data: T? = null
        constructor(message:Exception, _data: T): this(message){
            data = _data
        }
    }
}
