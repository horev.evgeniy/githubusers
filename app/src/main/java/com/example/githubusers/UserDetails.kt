package com.example.githubusers

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import by.kirich1409.viewbindingdelegate.viewBinding
import com.bumptech.glide.Glide
import com.example.githubusers.databinding.UserDetailsFragmentBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.user_details_fragment.view.*

@AndroidEntryPoint
class UserDetails : Fragment(R.layout.user_details_fragment) {

    private val viewModel: UserDetailsViewModel by viewModels()
    val args: UserDetailsArgs by navArgs()
    lateinit var user: User
    private val binding: UserDetailsFragmentBinding by viewBinding()




    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val userName = args.arg
        viewModel.getUser(userName)
        viewModel.user.observe(viewLifecycleOwner){user ->
            when (user){
                is Resource.Success ->{
                    binding.tvName.text = user.data.name
                    val avatar = binding.ivAvatar
                    val avaUrl = user.data.avatarUrl
                    binding.tvAccDate.text = user.data.createdAt
                    val link = user.data.organizationsUrl
                    binding.tvOrg.text = link
                    binding.tvEmail.text = user.data.email
                    Glide.with(avatar.context).load(avaUrl).into(avatar)
                }
                is Resource.NetError -> {
                    val toast = Toast.makeText(context, "net error", Toast.LENGTH_LONG)
                    toast.show()

                }
            }
        }







    }

}