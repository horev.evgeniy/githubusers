package com.example.githubusers
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import by.kirich1409.viewbindingdelegate.viewBinding
import com.bumptech.glide.Glide
import com.example.githubusers.databinding.ItemLayoutBinding

class Adapter (private val user: ArrayList<User>, private val listener: onItemClickListener): RecyclerView.Adapter<Adapter.ViewHolder>(){
    class   ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        private val binding: ItemLayoutBinding by viewBinding()
        fun bind( user: User, listener: onItemClickListener){
            binding.apply {
                Glide.with(avatar.context).load(user.avatarUrl).into(avatar)
                login.text = user.login
                userId.text = user.id
                userItem.setOnClickListener {
                    listener.onClick(user.login) }


            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_layout,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(user[position], listener)
    }

    override fun getItemCount(): Int {
        return user.size
    }

    interface onItemClickListener{
        fun onClick(id: String)
    }

    fun addUsers(newUser: List<User>?){
        user.clear()
        if (newUser != null) {
            user.addAll(newUser)
        }
        notifyDataSetChanged()
    }

}