package com.example.githubusers.DB

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.githubusers.User

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUsers(users: List<User>)

    @Query("SELECT * FROM user_table GROUP BY id")
    suspend fun getUsers(): List<User>

    @Query("SELECT * FROM user_table WHERE id =:userId")
    suspend fun getUser(userId: String): User
}