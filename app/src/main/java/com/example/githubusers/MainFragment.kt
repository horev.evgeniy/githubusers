package com.example.githubusers

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModel
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.githubusers.databinding.MainFragmentBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.concurrent.thread

@AndroidEntryPoint
class MainFragment : Fragment(R.layout.main_fragment) {

    private lateinit var adapter: Adapter
    private val binding: MainFragmentBinding by viewBinding()
    private val viewModel: MainViewModel by viewModels()


    val listener = object : Adapter.onItemClickListener {
        override fun onClick(login: String) {
            val action = MainFragmentDirections.actionMainFragmentToUserDetails(login)
            findNavController().navigate(action)
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.main_fragment, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recyclerView.layoutManager = LinearLayoutManager(context)
        adapter = Adapter(arrayListOf(), listener)
        binding.recyclerView.adapter = adapter

        binding.btnRefresh.setOnClickListener {
            viewModel.load()
        }
        setupObserver()
    }

    private fun setupObserver() {
        viewModel.users.observe(viewLifecycleOwner) { newUser ->
            when (newUser) {
                is Resource.Loading -> {
                    binding.progressBar.visibility = View.VISIBLE
                    binding.recyclerView.visibility = View.GONE
                }
                is Resource.DbError ->{
                    binding.progressBar.visibility = View.GONE

                    lifecycleScope.launch {
                    val toast = Toast.makeText(context, "net error", Toast.LENGTH_SHORT)
                        delay(3000)
                        toast.show()
                    }

                    val toast = Toast.makeText(context, newUser.dbMessage, Toast.LENGTH_LONG)
                    toast.show()

                    binding.btnRefresh.visibility = View.VISIBLE

                }
                is Resource.Success -> {
                    binding.btnRefresh.visibility = View.GONE
                    binding.progressBar.visibility = View.GONE
                    binding.recyclerView.visibility = View.VISIBLE
                    adapter.addUsers(newUser.data)
                }
                is Resource.NetError -> {
                    binding.progressBar.visibility = View.GONE
                    binding.recyclerView.visibility = View.VISIBLE

                    Log.v("net error", newUser.message.stackTraceToString())

                    adapter.addUsers(newUser.data)

                    val toast = Toast.makeText(context, "net error", Toast.LENGTH_LONG)
                    toast.show()

                }
            }


        }
    }

}