package com.example.githubusers

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "user_table")
data class User(

    @SerializedName("login")
    @ColumnInfo(name = "login")
    val login: String,

    @SerializedName("id")
    @PrimaryKey
    val id: String,

    @SerializedName("avatar_url")
    @ColumnInfo(name = "avatar_url")
    val avatarUrl: String,

    @SerializedName("url")
    @ColumnInfo(name = "url")
    val url: String,

    @SerializedName("organizations_url")
    @ColumnInfo(name = "organizations_url")
    val organizationsUrl: String,


    @SerializedName("name")
    @ColumnInfo(name = "name")
    val name: String?=null,


    @SerializedName("company")
    @ColumnInfo(name = "company")
    val company: String?=null,


    @SerializedName("email")
    @ColumnInfo(name = "email")
    val email: String?=null,



    @SerializedName("created_at")
    @ColumnInfo(name = "created_at")
    val createdAt: String?=null,


    @SerializedName("updated_at")
    @ColumnInfo(name = "updated_at")
    val updatedAt: String?= null
){}
