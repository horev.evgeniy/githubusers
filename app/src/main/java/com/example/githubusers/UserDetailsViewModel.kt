package com.example.githubusers


import android.util.Log
import androidx.lifecycle.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class UserDetailsViewModel @Inject constructor (private val repository: Repository) : ViewModel() {
    //private val appDatabase = AppDatabase.getInstance(application)
    //private val repository = Repository(UserApi.retrofitService, appDatabase)
    private val _user = MutableLiveData<Resource<User>>()
    val user: LiveData<Resource<User>> = _user

    fun getUser(userName: String) {
        viewModelScope.launch {
           try {
               val result = repository.getUser(userName)
               _user.value = Resource.Success(result)
           } catch (exception: Exception){
               Log.v("userDetails request", exception.stackTraceToString())
               _user.value = Resource.NetError(exception)
           }
        }

    }
}



