package com.example.githubusers


import com.example.githubusers.DB.AppDatabase
import com.example.githubusers.DB.UserDao
import com.example.githubusers.api.ApiService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Repository @Inject constructor (private val apiService: ApiService, private val userDao: UserDao){

    suspend fun getUsers(): List<User> {
        return apiService.getUsers()
    }

    suspend fun getUser(userName: String): User{
        return apiService.getUser(userName)
    }

    suspend fun insertUsers(users: List<User>){
        userDao.insertUsers(users)
    }

    suspend fun getUsersFromDb(): List<User>{
        return userDao.getUsers()
    }

}